%% Setup
% 
% Description:
%   This function adds the program paths and compiles the mex files.
%
% Contact:
%   Michael Villamizar
%   mvillami-at-iri.upc.edu
%   Institut de Robòtica i Informática Industrial CSIC-UPC
%   Barcelona - Spain
%   2016
%

%% main function
function prg_setup()
clc,close all,clear all

% messages
fun_messages('Program Setup','presentation');
fun_messages('Setup','title');

% root path
fun_messages('Adding program paths','process');
[root,~,~] = fileparts(mfilename('fullpath'));

% download pLSA code from Visual Geometry Group - University of Oxford
url = 'http://www.robots.ox.ac.uk/~vgg/software/pLSA/pLSA_demo.tgz';
fun_messages('downloading pLSA code','information');
untar(url,'./files/tools/pLSA_demo')

% add paths: root and mex files
fun_messages('Adding root','information');
addpath(root);
fun_messages('Adding files','information');
addpath(fullfile(root,'/files'));
fun_messages('Adding functions files','information');
addpath(fullfile(root,'/files/functions'));
fun_messages('Adding datasets files','information');
addpath(fullfile(root,'/files/datasets'));
fun_messages('Adding pLSA files','information');
addpath(fullfile(root,'/files/tools/pLSA_demo'));
fun_messages('Adding mex files','information');
addpath(fullfile(root,'/mex'));

% root path
cd(root);

% compile mex files
cd('./mex/');
fun_messages('Compiling mex files','process');
fun_messages('Compiling mex_img2II.cc','information');
mex mex_img2II.cc;
fun_messages('Compiling mex_II2Img.cc','information');
mex mex_II2Img.cc;
fun_messages('Compiling mex_fern_maps.cc','information');
mex mex_fern_maps.cc;
fun_messages('Compiling mex_HOG.cc','information');
mex mex_HOG.cc;
fun_messages('Compiling mex_rcfs_test.cc','information');
mex mex_rcfs_test.cc;
cd('../');

% message
fun_messages('End','title');

end

%% Messages
% This function prints a specific message on the command window.
function fun_messages(text,message)
if (nargin~=2), error('Incorrect input parameters'); end

% types of messages
switch (message)
    case 'presentation'
        fprintf('****************************************************\n');
        fprintf(' %s\n',text);
        fprintf('****************************************************\n');
        fprintf(' Michael Villamizar\n mvillami@iri.upc.edu\n');
        fprintf(' http://www.iri.upc.edu/people/mvillami/\n');
        fprintf(' Institut de Robòtica i Informàtica Industrial CSIC-UPC\n');
        fprintf(' c. Llorens i Artigas 4-6\n 08028 - Barcelona - Spain\n 2016\n');
        fprintf('****************************************************\n\n');
    case 'title'
        fprintf('****************************************************\n');
        fprintf('%s\n',text);
        fprintf('****************************************************\n');
    case 'process'
        fprintf('-> %s\n',text);
    case 'information'
        fprintf('->     %s\n',text);
    case 'warning'
        fprintf('-> %s !!!\n',text);
    case 'error'
        fprintf(':$ ERROR : %s\n',text);
        error('Program error');
end
end
