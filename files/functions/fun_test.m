%% Test
% This function tests the Random Clustering Ferns (RCFs) on test images.
function fun_test(RCFs)
if (nargin~=1), fun_messages('Incorrect input parameters','error'); end

% messages
fun_messages('Test','title');

% parameters
prms = fun_parameters();  % program parameters
imgFmt = prms.test.imgFmt;  % image format
numImgs = prms.test.numImgs;  % num. images
imgPath = prms.test.imgPath;  % images path
intThr = prms.runtime.intThr;  % intersection area -overlapping- threshold
detThr = prms.runtime.detThr;  % detection threshold
detThick = prms.runtime.detThick;  % detection rectangle thickness

% load images
imgFiles = dir([imgPath,'*',imgFmt]);

% num. images
numImgs = min(numImgs,size(imgFiles,1));

% variable
timSum = 0;  % times

% messages
fun_messages('Test images','process');
fun_messages(sprintf('Num. images: %d',numImgs),'information');

% images
for iterImg = 1:numImgs
    
	% times
	tic;

	% current image
	name = imgFiles(iterImg).name;                         
	img = imread([imgPath,name]);

	% detection image 
	detImg = fun_image_color(img,'RGB');

	% object detection
	dets = fun_detection(img,RCFs,detThr);

	% detection boxes and scores
	boxes = dets.boxes; 
	scores = dets.scores;
	clusters = dets.clusters;

	% detection results
	detResults.name = name;  % image file name
	detResults.times = dets.times;  % detection times
	detResults.boxes = dets.boxes;  % detection bounding boxes
	detResults.scores = dets.scores;  % detection scores
	detResults.clusters = dets.clusters;  % detection scores
    
	% save detection results
	fun_data_save(detResults,'./detections/data/',sprintf('%s.mat',name));

	% non-maxima supression
	[boxes,scores,clusters] = fun_non_maxima_suppression(boxes,scores,...
		clusters,intThr);

	% max. detection score
	maxScore = max(scores);

	% show detections
	for iterBox = 1:size(boxes,1);
		detImg = fun_draw_rectangle(detImg,boxes(iterBox,:),...
			fun_colors(clusters(iterBox)-1),detThick);
	end

	% write detection image
	imwrite(detImg,sprintf('./detections/imgs/%s',name));
    
	% times
	timSum = timSum + toc;  
	timAve = timSum/iterImg;
	timRes = (numImgs-iterImg)*timAve/60; 

	% message
	fun_messages(sprintf('Img: %d/%d - max. score: %.3f - time: %.3f',...
		iterImg,numImgs,maxScore,timRes),'information');
end
end

%% Draw rectangle
% This function draws a rectangle on the image.
function output = fun_draw_rectangle(img,location,color,thick)
if (nargin~=4), fun_messages('Incorrect input parameters','error'); end

% location 
location = round(location);

% check image limits
if (isempty(location)==0)
    
	% image size
	sy = size(img,1);
	sx = size(img,2);

	% rectangle coordinates
	left = location(1);
	top = location(2);
	right = location(3);
	bottom = location(4);

	% horizontal lines
	for iterX = min(max(1,left),sx):min(max(1,right),sx)
		for iterThick = 0:thick-1
			tmpa = min(max(1,top+iterThick),sy);
			tmpb = min(max(1,bottom+iterThick),sy);
			img(tmpa,iterX,:) = color;
			img(tmpb,iterX,:) = color;
		end
	end 

	% vertical lines
	for iterY = min(max(1,top),sy):1:min(max(1,bottom),sy)
		for iterThick = 0:thick-1
			tmpa = min(max(1,left+iterThick),sx);
			tmpb = min(max(1,right-iterThick),sx);
			img(iterY,tmpa,:) = color;
			img(iterY,tmpb,:) = color;
		end
	end 
end

% output
output = img;
end

%% Colors
% This function defines a set of colors used for visualization.
function output = fun_colors(index)
if (nargin~=1), fun_messages('Incorrect input parameters','error'); end

% the first color is used to show errors
index = index +1;

% select color
switch (index)
    case 1
        color = [1.0,0,0];
    case 2
        color = [0.0,1.0,0.0];
    case 3
        color = [0.0,0.0,1.0];
    case 4
        color = [1.0,1.0,0.0];
    case 5
        color = [0.0,1.0,1.0];
    case 6
        color = [1.0,0.0,1.0];
    case 7
        color = [1.0,0.5,0.8];
    case 8
        color = [0.5,1.0,0.8];
    case 9
        color = [0.8,0.5,1.0];
    case 10
        color = [0.3,0.7,0.3];
    case 11
        color = [0.7,0.3,0.3];
    case 12
        color = [0.3,0.3,0.7];
    case 13
        color = [0.8,0.4,0.0];
    case 14
        color = [0.0,0.8,0.4];
    case 15
        color = [0.4,0.0,0.8];
    case 16
        color = [0.8,0.0,0.4];
    case 17
        color = [0.8,0.8,0.8];
    case 18
        color = [0.2,0.2,0.2];
    case 19
        color = [0.5,0.5,1.0];
    case 20
        color = [1.0,0.5,0.5];
    otherwise
        color = [0.5,1.0,0.5];
        fun_messages('No more colors available','warning');
end

% output
output = color;
end
