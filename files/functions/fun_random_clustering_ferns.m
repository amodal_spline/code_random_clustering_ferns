% Random Clustering Rerns (RCFs)
% The rcfs combine Boosted Random Ferns (BRFs) and probabilistic Latent Semantic
% Analysis (pLSA) to perform object recognition and intra-class clustering using
% tree-structured visual words.
function output = fun_random_clustering_ferns()
    
% message
fun_messages('Random Clustering Ferns (RCFs)','title');
        
% load/compute rcfs
try
    % load the previous RCFs
    RCFs = fun_data_load('./variables/','RCFs.mat');

    % message
    fun_messages('the rcfs were loaded successfully','information');
catch ME
    
    % shared random ferns
    ferns = fun_random_ferns();
        
    % training samples
    samples = fun_samples();

    % classifier: Boosted Random Ferns (BRFs)
    tic;
    classifier = fun_classifier_brfs(samples,ferns);
    times.classifier = toc;		

    % clustering: probabilistic Latent Semantic Analysis (pLSA)
    tic;
    clusters = fun_clustering(classifier,samples);
    times.clustering = toc;	

    % random clustering ferns
    RCFs.times = times;  % times
    RCFs.clusters = clusters;  % intra-class clusters    
    RCFs.classifier = classifier;  % object classifier

    % save
    fun_data_save(RCFs,'./variables/','RCFs.mat');
    
end

% output
output = RCFs;
end

%% Shared random ferns 
% This function computes the shared random ferns used to compute the weak
% classifiers of the boosted random ferns classifier. Each fern corresponds to
% a set of binary features, and each feature is a -signed- comparision between
% two points (e.g pixels intensities). This function computes -at random- the 
% location (i.e y, x, feature channel) of these points.
function output = fun_random_ferns()

% message
fun_messages('Shared random ferns','process');

% load/compute the random ferns
try
	% load previous random ferns
	ferns = fun_data_load('./variables/','ferns.mat');

	% message
	fun_messages('The ferns were loaded successfully','information');
catch ME

	% parameters
	prms = fun_parameters();  % program parameters                                         
	numFerns = prms.ferns.numFerns;  % num. random ferns                             
	numFeats = prms.ferns.numFeats;  % num. binary features per fern                              
	fernSize = prms.ferns.fernSize;  % fern size -spatial squared size-

	% num. image channels (RGB)
	numChans = 3;

	% messages
	fun_messages(sprintf('Num. ferns: %d',numFerns),'information');
	fun_messages(sprintf('Num. features: %d',numFeats),'information');
	fun_messages(sprintf('Size: [%d %d]',fernSize,fernSize),'information');
	fun_messages(sprintf('Num. channels: %d',numChans),'information');

	% allocate memory
	data = zeros(numFeats,6,numFerns);

	% random ferns
	for iterFern = 1:numFerns

		% Binary feature: Feature value comparison between two random
	        % points A & B. The points (e.g pixels) are chosen at random.
		ya = floor(rand(numFeats,1)*fernSize);  % point A: location y
		xa = floor(rand(numFeats,1)*fernSize);  % point A: location x
		ca = floor(rand(numFeats,1)*numChans);  % point A: channel
		yb = floor(rand(numFeats,1)*fernSize);  % point B: location y    
		xb = floor(rand(numFeats,1)*fernSize);  % point B: location x
		cb = floor(rand(numFeats,1)*numChans);  % point B: channel

		% check fern values
		ya = min(fernSize-1,max(0,ya));
		xa = min(fernSize-1,max(0,xa));
		ca = min(numChans-1,max(0,ca));
		yb = min(fernSize-1,max(0,yb));
		xb = min(fernSize-1,max(0,xb));
		cb = min(numChans-1,max(0,cb));

		% fern data
		data(:,:,iterFern) = [ya,xa,ca,yb,xb,cb];

	end

	% random ferns
	ferns.data = data;  % ferns data                                          
	ferns.numFerns = numFerns;  % num. random ferns                              
	ferns.numFeats = numFeats;  % num. binary features                            
	ferns.fernSize = fernSize;  % fern size                              
	ferns.numChans = numChans;  % num. image  channels -fern depth-                       

	% save
	fun_data_save(ferns,'./variables/','ferns.mat');

end 

% output
output = ferns;
end

%%  Clustering
% This function performs clustering using probabilistic Latent Semantic 
% Analysis (pLSA) over the positive training images. The clustering is done
% using the response of the BRFs classifier as visual words. The pLSA allows
% to discover important intra-class modes, where each one corresponds to 
% particular object appearance.
function output = fun_clustering(BRFs,samples)
if (nargin~=2), fun_messages('Incorrect input parameters','error'); end

% parameters
prms = fun_parameters();  % program parameters
fs = prms.visualization.fontSize;  % font size
lw = prms.visualization.lineWidth;  % line width
minLog = prms.clustering.minLog;  % EM stopping condition: min. log-likelihood
showPlots = prms.clustering.showPlots; % ploting verbosity
numClusters = prms.clustering.numClusters; % num. clusters
numMaxIters = prms.clustering.numMaxIters;  % max. number of iterations of EM

% messages
fun_messages('Clustering: probabilistic Latent Semantic Analysis (pLSA)',...
	'process');
fun_messages(sprintf('Num. clusters: %d',numClusters),'information');
fun_messages(sprintf('Num. iterations: %d',numMaxIters),'information');

% variables
numBins = size(BRFs.hstms,2);

% Ferns response maps: This function computes the responses of the shared random
% ferns on the positive training samples. This is done by convolving the shared 
% ferns over the positive images.
fernMaps = mex_fern_maps(samples.positives,BRFs.ferns.data,BRFs.ferns.fernSize);

% Weak classifier outputs: This function tests the weak classifiers of the BRFs
% classifier on the positive training images (using the fern maps).
outs = fun_test_potential_weak_classifiers(BRFs.WCs,fernMaps);

% Visual words: This computes the tree-structured visual words on the positive
% samples. They are computed using the output of the weak classifiers (boosted
% random ferns).
words = zeros(size(outs,2),numBins*size(outs,1));
for iterSample = 1:size(outs,2)
	for iterFern = 1:size(outs,1)
		% fern output
		z = outs(iterFern,iterSample);
		% visual word
		w = numBins*(iterFern-1) + z;
		% save
		words(iterSample,w) = 1;
	end
end

% Clustering: This performs clustering using probabilistic Latent Semantic 
% Analysis (pLSA) over a matrix of visual words computed on the positive images.
% The pLSA discovers the latent variables which correspond to relevant object 
% appearances.
plsaPrms.Leps = minLog; % EM stopping condition: min. change in log-likelihood
plsaPrms.doplot = showPlots; % ploting verbosity
plsaPrms.maxit = numMaxIters; % maximum number of iterations of EM
[Pw_t,Px_t,Pt,Li] = pLSA_EM(words'+eps,numClusters,plsaPrms);

% show results
figure,subplot(221),imagesc(words),title('Visual Words','fontsize',fs);
xlabel('Words','fontsize',fs),ylabel('Samples','fontsize',fs);
subplot(222),imagesc(Pw_t),title('p(w|t)','fontsize',fs);
xlabel('Clusters','fontsize',fs),ylabel('Words','fontsize',fs);
subplot(223),imagesc(Px_t),title('p(x|t)','fontsize',fs);
xlabel('Clusters','fontsize',fs),ylabel('Samples','fontsize',fs);
subplot(224),plot(Pt,'kx-','linewidth',lw),title('p(t)','fontsize',fs);
xlabel('Clusters','fontsize',fs),ylabel('Prob.','fontsize',fs);
%figure,plot(Li,'k-','linewidth',lw),xlabel('Li','fontsize',fs);

% plsa data
clusters.Pt = Pt;
clusters.Pw_t = Pw_t;
clusters.Px_t = Px_t;

% clustering: for visualization sing tsne
%lll = zeros(100,1)
%for i = 1:size(Px_t,1)
%[~,c]=max(Px_t(i,:));
%lll(i) = c;
%end
%lll
%data.plsa_labels = lll;
%data.samples = samples;
%data.words = words;
%fun_data_save(data,'./results/','clustering_data.mat');
%pause

% output
output = clusters;
end
