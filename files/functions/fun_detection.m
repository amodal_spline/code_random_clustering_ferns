%% Detection
% This function performs object detection using the proposed Random Clustering
% Ferns (RCFs). The BRFs classifier is tested in the input image using a 
% sliding window approach. The pLSA clustering is applied to the output of the
% BRFs classifier to identify the intra-class appearance cluster. This function
% makes use of mex files for speeding up the detection process.
function output = fun_detection(img,RCFs,detThr)
if (nargin~=3), fun_messages('Incorrect input parameters','error'); end

% parameters
prms = fun_parameters();  % parameters
imgExt = prms.runtime.imgExt;  % additional image extend
minCell = prms.runtime.minCellSize;  % min. cell size
maxCell = prms.runtime.maxCellSize;  % max. cell size
imgHeight = prms.runtime.imgHeight;  % standard image height
numMaxDets = prms.runtime.numMaxDets; % num. max. detections
minImgSize = prms.runtime.minImgSize;  % min. image size
numOutputs = prms.runtime.numMaxOutputs;  % num. max. classifier outputs
numImgLevels = prms.runtime.numImgLevels; % num. image levels (pyramid)

% random clusting ferns
BRFs = RCFs.classifier;  % BRFs classifier
pLSA = RCFs.clusters;  % pLSA clustering

% num. intra-class clusters
numClusters = size(pLSA.Pw_t,2);

% object size
objSize = BRFs.objSize;

% extend the image -for objects partially out the image-
img = fun_image_extend(img,imgExt);

% resize the image
rszFact = size(img,1)/imgHeight; % resize factor
img = imresize(img,inv(rszFact)); % resize image
imgSize = size(img);  % scaled image size

% num. octaves and scales
numOctaves = min(floor(log2(imgSize(1:2)) - log2(minImgSize)));
numScales = numOctaves*numImgLevels;

% variables
count = 0;  % counter
timImg = 0;  % times: image processing    
timRCFs = 0;  % times: RCFs testing
timFerns = 0;  % times: ferns comvolution

% allocate
detBoxes = zeros(numMaxDets,4);  % detection boxes
detScores = zeros(numMaxDets,1);  % detection scores
detClusters = zeros(numMaxDets,1);  % detection clusters

% initial time 
t0 = cputime;

% image scales
for iterScale = 0:numScales
   
    % time variable
    t1 = cputime;
    
    % image size
    scaRatio = 2^(iterScale/numImgLevels);  % scale ratio
    newSize = round(imgSize./scaRatio);  % new image size
    
    % scaled image
    %scaImg = imresize(img,newSize(1:2),'bilinear');
    scaImg = imresize(img,newSize(1:2));

    % RGB images
    featImg = fun_image_color(scaImg,'RGB');
   
    % integral image computation
    II = mex_img2II(featImg);
    
    % update image processing time
    timImg = timImg  + cputime - t1;
    
    % image cells
    for iterCell = minCell:maxCell
        
        % scale factor
        scaFact = iterCell*scaRatio*rszFact;
       
        % current image level
        t1 = cputime; 
        cImg = mex_II2Img(II,iterCell);
        timImg = timImg  + cputime - t1;
        
        % small image size
        if (size(cImg,1)<minImgSize || size(cImg,2)<minImgSize), break; end
        
        % ferns convolution maps
        t1 = cputime;
        fernMaps = mex_fern_maps(cImg,BRFs.ferns.data,BRFs.ferns.fernSize);
        timFerns = timFerns  + cputime - t1;
        
        % test RCFs
        t1 = cputime;
        [outputs, ~] = mex_rcfs_test(fernMaps,BRFs.WCs,BRFs.hstms,objSize,...
		numOutputs,numClusters,pLSA.Pw_t,pLSA.Pt);
        timRCFs = timRCFs  + cputime - t1;
        
        % detections
        for iterDet = 1:numOutputs
            
            % detection score and cluster
            score = outputs(iterDet,3);  
            cluster = outputs(iterDet,4);
            
            % this test rejects detections with lower scores
            if (score<detThr), break; end
          
            % update counter
            count = count + 1;
            
            % detection location
            y = outputs(iterDet,1);
            x = outputs(iterDet,2);
            
            % bounding box
            box = round([x, y, x + objSize(2) - 1, y + objSize(1) - 1]...
	    	*scaFact - imgExt);
         
            % detection data: score, bounding box and cluster
            detBoxes(count,:) = box;
            detScores(count,:) = score;
            detClusters(count,:) = cluster;
        end
    end
end

% sort detections by score
detBoxes = detBoxes(1:count,:);
detScores = detScores(1:count,:);
detClusters = detClusters(1:count,:);
detData = sortrows([detScores,detBoxes,detClusters],-1);
detBoxes = detData(:,2:5);
detScores = detData(:,1);
detClusters = detData(:,6);

% total time
timTotal = cputime - t0;

% time data
timData.RCFs = timRCFs;
timData.image = timImg;
timData.ferns = timFerns;
timData.total = timTotal;

% output detections
dets.times = timData;
dets.boxes = detBoxes; 
dets.scores = detScores; 
dets.numDets = count;
dets.clusters = detClusters;

% output
output = dets;
end

%% Extend image
% This function extends the image in order to consider objects partially out of
% the image during the detection. The added information corresponds to border 
% rows and columns of the original image.
function output = fun_image_extend(img,ext)
if (nargin~=2), fun_messages('Incorrect input parameters','error'); end

% image color
if (size(img,3)~=3)

	% gray-level image
	outImg = img;

	% extension
	for iter = 1:ext
		left = outImg(:,1);
		right = outImg(:,end);
		outImg = [left,outImg,right];
		top = outImg(1,:);
		bottom = outImg(end,:);
		outImg = [top;outImg;bottom];
	end
else

	% color image
	outImg = img;

	% extension
	for iter = 1:ext
		left = outImg(:,1,:);
		right = outImg(:,end,:);
		outImg = [left,outImg,right];
		top = outImg(1,:,:);
		bottom = outImg(end,:,:);
		outImg = [top;outImg;bottom];
	end
end

% output
output = outImg;
end
