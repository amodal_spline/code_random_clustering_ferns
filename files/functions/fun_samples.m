%% Samples
% This function extracts the training samples used to compute the random 
% clustering ferns. The object dataset includes positive -object- and negative 
% -background- image samples.
function output = fun_samples()

% message
fun_messages('Image samples','process');

% load/extract image samples
try 
	% load previous training samples
	samples = fun_data_load('./variables/','samples.mat');
    
	% message
	fun_messages('The training samples were loaded successfully',...
		'information');
catch ME
    
	% parameters
	prms = fun_parameters();  % program parameters
	imgSize = prms.classifier.objSize;  % image size -object size-
	posImgFmt = prms.train.posImgFmt;  % positive image format
	negImgFmt = prms.train.negImgFmt;  % negative image format
	numPosImgs = prms.train.numPosImgs;  % num. positive images
	numNegImgs = prms.train.numNegImgs;  % num. negative images
	posImgPath = prms.train.posImgPath;  % positive images path
	negImgPath = prms.train.negImgPath;  % engative images path

	% load positive and negative images
	posImgs = fun_load_images(posImgPath,numPosImgs,posImgFmt,imgSize);
	negImgs = fun_load_images(negImgPath,numNegImgs,negImgFmt,imgSize);

	% num. positive and negative samples
	numPosSamples = size(posImgs,4);
	numNegSamples = size(negImgs,4);

	% image samples
	samples.imgSize = imgSize;
	samples.positives = posImgs;
	samples.negatives = negImgs;
	samples.numPosSamples = numPosSamples;
	samples.numNegSamples = numNegSamples;

	% save
	fun_data_save(samples,'./variables/','samples.mat');

	% message
	fun_messages(sprintf('Num. positive image samples: %d',...
		numPosSamples),'information');
	fun_messages(sprintf('Num. negative image samples: %d',...
		samples.numNegSamples),'information');
	fun_messages(sprintf('Image size: [%d %d]',imgSize(:)),'information');
end

% output
output = samples;
end

%% Load images
% This function loads images from files. The images are scaled and proccesed 
% -feature extraction- according to the program parameters.
function output = fun_load_images(imgPath,numImgs,imgFmt,imgSize)
if (nargin~=4), fun_messages('Incorrect input parameters','error'); end

% image files
imgFiles = dir([imgPath,'*',imgFmt]);

% num. image samples
numSamples = min(numImgs,size(imgFiles,1));

% check
if (numSamples==0), fun_messages('No images in this path','error'); end

% num. image channels (RGB)
numChannels = 3;

% allocate
samples = zeros([imgSize,numChannels,numSamples]);

% image samples
for iterImg = 1:numSamples
    
    % current image
    img = imread([imgPath,imgFiles(iterImg).name]);
    
    % image resize   
    img = imresize(img,imgSize(1:2),'bilinear');

    % RGB images
    img = fun_image_color(img,'RGB');

    % save
    samples(:,:,:,iterImg) = img;
end

% output
output = samples;
end
