%% Image normalization
% This function normalizes the input image between [0,1].
function output = fun_image_normalize(img)
if (nargin~=1), fun_messages('Incorrect input parameters','error'); end

% normalize
img = double(img);
img = img - min(img(:));
img = img./max(img(:));

% output
output = img;
end
