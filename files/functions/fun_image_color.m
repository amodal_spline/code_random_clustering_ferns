%% Image color
% This function converts the input image to a specific color format.
function output = fun_image_color(img,imgColor)
if (nargin~=2), fun_messages('Incorrect input parameters','error'); end

% color space
switch (imgColor)
    % gray-scale image -gray-
    case 'GRAY'
        % convert RGB image to gray-scale image
        if (size(img,3)==3), img = rgb2gray(img);end
    % RGB image
    case 'RGB'
        % convert gray-scale image to RGB
        if (size(img,3)==1),
            tmp = cat(3,img,img);
            img = cat(3,tmp,img);
        end
    % HSV image
    case 'HSV'
        % check
        if (size(img,3)==1),fun_messages('The input image is not RGB',...
		'error'); end
        % HSV image
        img = rgb2hsv(img);
    otherwise
        fun_messages('Incorrect color mode','error');
end

% normalization
img = fun_image_normalize(img);

% output
output = img;
end
