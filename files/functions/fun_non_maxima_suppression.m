%% Non-maxima suppression
% This function removes non-maxima detection hypotheses -boxes-. They are
% suppressed according to their scores and an insertection area criterion 
% -intThr-, which measure overlapping between bounding boxes.
function [output1,output2,output3] = fun_non_maxima_suppression(boxes,scores,...
	clusters,intThr)
if (nargin~=4), fun_messages('Incorrect input parameters','error'); end

% num. boxes
numBoxes = size(boxes,1);

% non-maxima supression
if (numBoxes>0)
    tmp = fun_nms([boxes,scores,clusters],intThr,scores);
    boxes = tmp(:,1:4);
    scores = tmp(:,5);
    clusters = tmp(:,6);
end

% output
output1 = boxes;
output2 = scores;
output3 = clusters;
end

%% non-Maxima Supression : Ramanan work
function top = fun_nms(boxes, overlap, iScores)

% top = nms(boxes, overlap)
% Non-maximum suppression.
% Greedily select high-scoring detections and eliminate overlaping ones.
if isempty(boxes)
    top = [];
else
    
    % bboxes coordinates
    x1 = boxes(:,1);
    y1 = boxes(:,2);
    x2 = boxes(:,3);
    y2 = boxes(:,4);
    
    % nose
    s  = boxes(:,end);
    
    % bounding boxes araes
    area = (x2-x1+1).*(y2-y1+1);

    % sort bounding boxes by their scores
    [vals, I] = sort(iScores);
        
    pick = [];
    while ~isempty(I)
        
        % choose best score
        last = length(I);
        
        % save best score and eliminate from list
        i        = I(last);
        pick     = [pick; i];
        suppress = [last];
        
        % check rest bounding boxes
        for pos = 1:last-1
            
            % curren index box
            j = I(pos);
            
            % interserction coordinates
            xx1 = max(x1(i), x1(j));
            yy1 = max(y1(i), y1(j));
            xx2 = min(x2(i), x2(j));
            yy2 = min(y2(i), y2(j));
            
            % width and height : intersection area
            w = xx2-xx1+1;
            h = yy2-yy1+1;
            
            % if intersection area = test area -> reject : 0
            minor = sum(abs([xx1,yy1,xx2,yy2] - [x1(j),y1(j),x2(j),y2(j)]),2);
            
            if (w > 0) && (h > 0) 
                % compute overlap : intersection area / best bounding box area
                %o = w * h / area(j);
                o = (w*h) / ((x2(i)-x1(i)+1)*(y2(i)-y1(i)+1));
                if (o>overlap) || (minor==0)
                    suppress = [suppress; pos];
                end
            end
        end
        I(suppress) = [];
    end
    top = boxes(pick,:);
end
end
