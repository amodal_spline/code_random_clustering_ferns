%% Parameters
% This function defines the program parameters. They can be modified in the 
% experiment function in order to measure the impact of these parameters over 
% the learning and detection performance of the random clustering ferns.
function output = fun_parameters()

% experiments
prmsExp = fun_experiments();

% random ferns
fernSize = prmsExp.fernSize;  % fern size                  
numFerns = prmsExp.numFerns;  % num. random ferns                                          
numFeats = prmsExp.numFeats;  % num. fern features per fern                              
ferns = struct('numFeats',numFeats,'numFerns',numFerns,'fernSize',fernSize);

% train data
posImgFmt = '.*g';  % positive image format
negImgFmt = '.*g';  % negative image format
numPosImgs = 100;  % num. positive images
numNegImgs = 100;  % num. negative images
posImgPath = './datasets/caltech_faces/positives/';  % positive image files
negImgPath = './datasets/caltech_faces/negatives/';  % negative image files
train = struct('numPosImgs',numPosImgs,'numNegImgs',numNegImgs,'posImgPath',...
	posImgPath,'negImgPath',negImgPath,'posImgFmt',posImgFmt,'negImgFmt',...
	negImgFmt);

% test data
imgFmt = '.*g';  % image format
numImgs = 100;  % num. images
imgPath = './datasets/caltech_faces/images/';  % image files
test = struct('numImgs',numImgs,'imgPath',imgPath,'imgFmt',imgFmt);

% classifier: boosted random ferns
eps = 0.001;  % epsilon
numWCs = prmsExp.numWCs;  % num. Weak Classifiers (WCs)
objSize = prmsExp.size;  % object size
numMaxPWCs = 8000;  % num. max. Potential Weak Classifiers (PWCs)
classifier = struct('numWCs',numWCs,'objSize',objSize,'eps',eps,'numMaxPWCs',...
	numMaxPWCs);

% clustering: probabilistic latent semantic analysys
minLog = 0.1;  % EM stopping condition: minimum change in log-likelihood
showPlots = 0;  % ploting verbosity
numClusters = prmsExp.numClusters;  % num. clusters
numMaxIters = prmsExp.numIters;  % maximum number of iterations of EM
clustering = struct('minLog',minLog,'showPlots',showPlots,'numClusters',...
	numClusters,'numMaxIters',numMaxIters);

% runtime
detThr = 0.75;  % detection threshold
imgExt = 0;  % image file extension
intThr = 0.01;  % intersection area -overlapping- threshold    
detThick = 18;  % detection rectangle thickness
imgHeight = 240;  % image height    
minImgSize = 10;  % min. image size        
numMaxDets = 500;  % num. max. detections
minCellSize = 3;  % min. cell size        
maxCellSize = 50;  % max. cell size
numImgLevels = 2;  % num. image levels
numMaxOutputs = 10;  % num. maximum of detection outputs    
runtime = struct('numImgLevels',numImgLevels,'minCellSize',minCellSize,...
	'maxCellSize',maxCellSize,'detThr',detThr,'minImgSize',minImgSize,...
	'numMaxOutputs',numMaxOutputs,'imgHeight',imgHeight,'intThr',intThr,...
	'imgExt',imgExt,'detThick',detThick,'numMaxDets',numMaxDets);
 
% visualization
fontSize = 18;  % font width
lineWidth = 4;  % line width
markerSize = 16;  % marker size
visualization = struct('markerSize',markerSize,'lineWidth',lineWidth,...
	'fontSize',fontSize);

% program parameters
prms.test = test;
prms.ferns = ferns;
prms.train = train;
prms.runtime = runtime;
prms.classifier = classifier;
prms.clustering = clustering;
prms.visualization = visualization;

% output
output = prms;
end
