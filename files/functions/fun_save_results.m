%% Save results
% This function saves the experiment results in a particular folder.
function fun_save_results()
    
% current experiment
prmsExp = fun_experiments();  % experiment paramaters
expTag = prmsExp.tag;  % experiment tag
  
% output directory    
mkdir(sprintf('./results/%s',expTag));    
mkdir(sprintf('./results/%s/detections/',expTag));    
mkdir(sprintf('./results/%s/detections/imgs/',expTag));    
mkdir(sprintf('./results/%s/detections/data/',expTag));    
mkdir(sprintf('./results/%s/variables/',expTag));    

% move variables
movefile('./variables/*.mat',sprintf('./results/%s/variables/',expTag));

% move detection files
movefile('./detections/imgs/*.png',sprintf('./results/%s/detections/imgs/',...
 	expTag));
movefile('./detections/data/*.mat',sprintf('./results/%s/detections/data/',...
	expTag));
end
