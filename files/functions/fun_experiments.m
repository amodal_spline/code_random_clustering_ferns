%% Experiments
% This function defines all the experiments used in the program. At each
% experiment, the random clustering ferns can be tested with different program 
% parameters (see fun_parameters.m).
function output = fun_experiments()

% global variables
global iterExp;

% experiments
switch (iterExp)
case 1
	data.tag = 'exp_t1';  % experiment tag
	data.size = [24,17];  % object size -height x width-
	data.numWCs = 300;  % num. weak classifiers
	data.numFerns = 10;  % num. shared random ferns
	data.numFeats = 7;  % num. binary features per fern
	data.fernSize = 7;  % fern size -pixels x pixels-
	data.numIters = 500;  %  num. EM iterations
	data.numClusters = 5;  % num. intra-class clusters
otherwise
	fun_messages('No more experiments','error');
end

% output
output = data;
end
