%% Delete variables
% This function removes the temporal variables.
function fun_delete_variables()
% delete variables
%delete './variables/ferns.mat';
%delete './variables/samples.mat';
%delete './variables/rcfs.mat';
% delete all variables
delete './variables/*.mat';
end
